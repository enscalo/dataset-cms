fabric.Door = fabric.util.createClass(fabric.Object, {
    type: 'door',
  
    x: 0,
    y: 0,
    radius: 0,
    startAngle: 0,
    endAngle: 0,
    direction: true,
    isBase: false,
    startx: 0,
    starty: 0,
    lineWidth: 5,
    stroke: "",
    position: "",
    retention: null,
  
    cacheProperties: fabric.Object.prototype.cacheProperties.concat('radius', 'startAngle', 'endAngle'),
  
    initialize: function (options) {
      options = options || {};
      this.callSuper('initialize', options);
  
      this.set('x', options.pivot[0]).set('y', options.pivot[1]);
      if (options.hasOwnProperty("lineWidth") == true) {
        this.set('lineWidth', options.lineWidth);
      }
      this.setRadius(options.start, options.pivot)
        .set('startAngle', this._determineStartAngle(options.start, options.pivot))
        .set('isBase', options.isBase || false)
        ._determineOpens();
      if (options.hasOwnProperty("stroke") == true) {
        this.set('stroke', options.stroke);
      }
    },
  
    setRadius: function (start, pivot) {
      return this.set('radius', Math.sqrt(Math.pow(start[0] - pivot[0], 2) + Math.pow(start[1] - pivot[1], 2)))
        .set('width', 2 * this.radius).set('height', 2 * this.radius);
    },
  
    _determineStartAngle: function (start, pivot) {
      var cos_component = (pivot[0] - start[0]) <= 0, sin_component = (pivot[1] - start[1]) <= 0;
      var angle = Math.acos(Math.abs((pivot[0] - start[0]) / this.radius)), sign = {true: 1, false: -1, 0: 0};
      var xnor = (!cos_component || sin_component) && (cos_component || !sin_component);
      this._determineRetention(xnor);

      return (!cos_component & true) * Math.PI + sign[xnor] * angle;
    },

    _determineOpens: function() {
      var opens = this.get('opens');
      if (opens == "left") {
        direction = "anticlockwise";
        this.set('endAngle', this.get('startAngle') - Math.PI / 2);
      } else if (opens == "right") {
        direction = "clockwise";
        this.set('endAngle', this.get('startAngle') + Math.PI / 2);
      }
      this.set('direction', (direction == "anticlockwise"));
      return this;
    },

    _determineRetention: function(startAngle) {
      var opens = this.get('opens'), position = this.get('position'), opensSign = {right: true, left: false}, 
      sign = {north: 1, south: -1, east: 1, west: -1}, _sign = {true: 1, false: -1};
      var xor = (!startAngle && opensSign[opens]) || (startAngle && !opensSign[opens]);

      this.set('retention', _sign[xor] * sign[position]);
      return this;
    },
  
    _render: function (ctx) {
      this._drawArc(ctx);
      this._drawHandle(ctx);
      if (this.isBase) {
        this._drawBase(ctx);
      }
    },
  
    _drawArc: function (ctx) {
      ctx.beginPath();
      ctx.arc(0, 0, this.radius, this.startAngle, this.endAngle, this.direction);
      ctx.stroke();
    },
  
    _drawHandle: function (ctx) {
      ctx.beginPath();
      ctx.moveTo(0, 0);
      ctx.lineWidth = 1;
      ctx.lineTo(Math.cos(this.endAngle) * this.radius, Math.sin(this.endAngle) * this.radius);
      ctx.stroke();
    },
  
    _drawBase: function (ctx) {
      ctx.beginPath();
      ctx.moveTo(0, 0);
      ctx.lineWidth = this.get('lineWidth');
      ctx.lineTo(Math.cos(this.startAngle) * this.radius, Math.sin(this.startAngle) * this.radius);
      ctx.stroke();
    },
  
    toObject: function (propertiesToInclude) {
      return this.callSuper('toObject', ['radius', 'startAngle', 'endAngle'].concat(propertiesToInclude));
    }
  });