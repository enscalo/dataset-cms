fabric.TransitElement = fabric.util.createClass(fabric.Object, {
  type: 'transit_element',

  x: 0,
  y: 0,
  to_x: 0,
  to_y: 0,
  lineWidth: 10,
  strokeStyle: "",
  position: null,
  retention: 0,

  cacheProperties: fabric.Object.prototype.cacheProperties.concat('x', 'y', 'to_x', 'to_y'),

  initialize: function (options) {
    options = options || {};
    this.callSuper('initialize', options);

    var fromModulus = Math.sqrt(Math.pow(options.pivot[0], 2) + Math.pow(options.pivot[1], 2)),
            toModulus = Math.sqrt(Math.pow(options.to_x, 2) + Math.pow(options.to_y, 2));
    this.set('x', options.pivot[0]).set('y', options.pivot[1])
            .set('to_x', options.to_x).set('to_y', options.to_y)
            .set('width', Math.abs(this.to_x - this.x)).set('height', Math.abs(this.to_y - this.y));
    if (fromModulus < toModulus) {
      this.set('left', options.pivot[0]).set('top', options.pivot[1]);
    } else {
      this.set('left', options.to_x).set('top', options.to_y);
    }
    if (options.hasOwnProperty("lineWidth") == true) {
      this.set('lineWidth', options.lineWidth);
    }
    if (options.hasOwnProperty("strokeStyle") == true) {
      this.set('strokeStyle', options.strokeStyle);
    }
  },

  _render: function (ctx) {
    this._drawLine(ctx, -(this.to_x - this.x) / 2, -(this.to_y - this.y) / 2);
  },

  _drawLine: function (ctx, x, y) {
    ctx.beginPath();
    ctx.moveTo(-x, -y);
    ctx.lineWidth = this.get('lineWidth');
    if(this.get('strokeStyle')) {
      ctx.strokeStyle = this.get('strokeStyle');
    }
    ctx.lineTo(x, y);
    ctx.stroke();
  },

  toObject: function (propertiesToInclude) {
    return this.callSuper('toObject', ['x', 'y', 'to_x', 'to_y'].concat(propertiesToInclude));
  }
});