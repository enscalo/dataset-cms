var CONFIG = CONFIG || {};

CONFIG['scale'] = 160;
CONFIG['offset'] = 400;
CONFIG['order'] = {
  "area": "window",
  "area_derived": "door"
};
CONFIG['positions'] = ["north", "east", "west", "south"];
CONFIG['position'] = {
  "north": 0,
  "south": 0,
  "west": 1,
  "east": 1
};
CONFIG['altposition'] = {
  "north": 1,
  "south": 1,
  "west": 0,
  "east": 0
};

/**
 * 
 * @param {type} canvas
 * @param {type} canvasSize
 * @param {type} fillColor
 * @param {type} figure
 * @returns {fabric.Rect}
 */
function createBoundedArea(canvas, canvasSize, fillColor, figure) {
  var padding = {"width": (canvasSize.width - figure.width) / 2, "height": (canvasSize.height - figure.height) / 2};
  var fillStyle = fillColor, fillRGB = fillStyle.replace("rgb", "").replace("(", "").replace(")", "").split(",");
  var strokeStyleRGB = [255 - (new Number(fillRGB[0])).valueOf(), 255 - (new Number(fillRGB[1])).valueOf(), 255 - (new Number(fillRGB[2])).valueOf()];
  var rect = new fabric.Rect({
    left: padding['width'],
    top: padding['height'],
    fill: fillColor,
    width: figure.width,
    height: figure.height,
    stroke: 'rgb(' + strokeStyleRGB[0].toString() + "," + strokeStyleRGB[1].toString() + "," + strokeStyleRGB[2].toString() + ")"
  });
  return rect;
}

/**
 * 
 * @param {type} key
 * @returns {CONFIG.positions}
 */
function getPositionFromKey(key)
{
  for (var i in CONFIG['positions']) {
    if (key.indexOf(CONFIG['positions'][i]) !== -1) {
      return CONFIG['positions'][i];
    }
  }
  return null;
}

function execute_coordinates_setting(rect, order, position_array, padding_height, padding_width)
{
  var coordinate_dimension, count = _.countBy(position_array, function(pos) {
    return pos;
  }), prev_key_coords_door = [rect.get('left'), rect.get('top')], prev_key_coords_window = [rect.get('left'), rect.get('top')], 
  prev_key_coords = null, temp_pos = null, coordinates = {};
  for (var key in order) {
    temp_pos = getPositionFromKey(key);
    if(prev_key_coords == null) {
      if(key.indexOf("door") !== -1) {
        prev_key_coords = prev_key_coords_door;
      } else {
        prev_key_coords = prev_key_coords_window;
      }
    }
    if (key.indexOf("east") !== -1 || key.indexOf("west") !== -1) {
      coordinate_dimension = key.indexOf("east") !== -1 ?
        rect.get('left') : rect.get('left') + rect.get('width');
      coordinates[key] = [[coordinate_dimension, prev_key_coords[1] + padding_height[temp_pos]],
        [coordinate_dimension, prev_key_coords[1] + padding_height[temp_pos] + order[key] / 2],
        [coordinate_dimension, prev_key_coords[1] + padding_height[temp_pos] + order[key]]];
    } else if (key.indexOf("north") !== -1 || key.indexOf("south") !== -1) {
      coordinate_dimension = key.indexOf("north") !== -1 ?
        rect.get('top') + rect.get('height') : rect.get('top');
      coordinates[key] = [[prev_key_coords[0] + padding_width[temp_pos], coordinate_dimension],
        [prev_key_coords[0] + padding_width[temp_pos] + order[key] / 2, coordinate_dimension],
        [prev_key_coords[0] + padding_width[temp_pos] + order[key], coordinate_dimension]];
    }
    
    if(count.hasOwnProperty(temp_pos) && count[temp_pos] > 1) {
      prev_key_coords = coordinates[key][2];
    } else {
      prev_key_coords = null;
    }
  }
  return coordinates;
}

/**
 * 
 * @param {type} rect
 * @param {type} area
 * @param {type} area_derived
 * @param {type} order
 * @param {type} position_array
 * @param {type} figure
 * @returns {createAccessForRect.boundedareaAnonym$11}
 */
function createAccessForRect(rect, area, area_derived, order, position_array, figure) {
  var length = 0, netarea = 0.0, door = [], windowElement = [], position_mapping = {"east": "height", "north": "width", "south": "width", "west": "height"};
  var fillStyle = rect.get('fill'), fillRGB = fillStyle.replace("rgb", "").replace("(", "").replace(")", "").split(",");
  var strokeStyleRGB = [255 - (new Number(fillRGB[0])).valueOf(),
    255 - (new Number(fillRGB[1])).valueOf(), 255 - (new Number(fillRGB[2])).valueOf()];
  var position, padding_height = {}, padding_width = {};
  for (var pos in position_array) {
    position = position_array[pos];
    if (area.hasOwnProperty(position)) {
      if(_.isArray(area[position])) {
        length += area[position].length;
        netarea += _.reduce(area[position], function (memo, num) {
          return (memo + num);
        }, 0);
      } else {
        length += 1;
        netarea += area[position];
      }
    }
    if (area_derived.hasOwnProperty(position)) {
      if(_.isArray(area_derived[position])) {
        length += area_derived[position].length;
        netarea += _.reduce(area_derived[position], function (memo, num) {
          return (memo + num);
        }, 0);
      } else {
        length += 1;
        netarea += area_derived[position];
      }
    }
    if (position.indexOf("east") !== false || position.indexOf("west") !== false) {
      padding_height[position] = 1.0 * ((figure[position_mapping[position]] - netarea) / (length + 1));
    }
    if (position.indexOf("north") !== false || position.indexOf("south") !== false) {
      padding_width[position] = 1.0 * ((figure[position_mapping[position]] - netarea) / (length + 1));
    }
  }
  var coordinates = execute_coordinates_setting(rect, order, position_array, padding_height, padding_width), 
  i = 0, temp_pos = null;
  for (var key in coordinates) {
    temp_pos = getPositionFromKey(key);
    var pivot_x, pivot_y, opens, start_x, start_y, to_x, to_y, top = null, left = null;
    if (key.indexOf("door") !== -1) {
      if ((i < 1 && (key.indexOf("east") !== -1)) ||
              (i >= 1 && (key.indexOf("west") !== -1)) || (i < 1 && (key.indexOf("north") !== -1))
              || (i >= 1 && (key.indexOf("south") !== -1))) {
        if (key.indexOf("north") !== -1 || key.indexOf("south") !== -1) {
          pivot_y = coordinates[key][0][1], pivot_x = coordinates[key][0][0];
          start_y = coordinates[key][2][1], start_x = coordinates[key][2][0];
        } else {
          pivot_y = coordinates[key][2][1], pivot_x = coordinates[key][2][0];
          start_y = coordinates[key][0][1], start_x = coordinates[key][0][0];
        }
        opens = "right";
      } else {
        if (key.indexOf("north") !== -1 || key.indexOf("south") !== -1) {
          pivot_y = coordinates[key][2][1], pivot_x = coordinates[key][2][0];
          start_y = coordinates[key][0][1], start_x = coordinates[key][0][0];
        } else {
          pivot_y = coordinates[key][0][1], pivot_x = coordinates[key][0][0];
          start_y = coordinates[key][2][1], start_x = coordinates[key][2][0];
        }
        opens = "left";
      }
      door.push(new fabric.Door({
        pivot: [pivot_x, pivot_y], start: [start_x, start_y], isBase: true, opens: opens, position: temp_pos,
        stroke: 'rgb(' + strokeStyleRGB[0].toString() + "," + strokeStyleRGB[1].toString() + "," + strokeStyleRGB[2].toString() + ")"
      }));
    } else if (key.indexOf("window") !== -1) {
      pivot_y = coordinates[key][0][1], pivot_x = coordinates[key][0][0];
      to_y = coordinates[key][2][1], to_x = coordinates[key][2][0];
      windowElement.push(new fabric.WindowElement({
        pivot: [pivot_x, pivot_y], to_x: to_x, to_y: to_y, position: temp_pos,
        strokeStyle: 'rgb(' + strokeStyleRGB[0].toString() + "," + strokeStyleRGB[1].toString() + "," + strokeStyleRGB[2].toString() + ")"
      }));
    }
    i++;
  }
  return {'door': door, 'windowElement': windowElement};
}