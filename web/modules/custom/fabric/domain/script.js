/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var canvas = null;

/**
 * 
 * @param {type} config
 * @returns {unresolved}
 */
function buildArea(config)
{
  if (!config.hasOwnProperty('area')) {
    throw new Error("config doesnot have area key");
  }
  var pos = null;
  for(var i in CONFIG['position']) {
    pos = CONFIG['position'][i];
    if(config['area'].hasOwnProperty(pos)) {
      config['area'][pos] = _.map(config['area'][pos], function(num) {
        return num * CONFIG['scale'];
      });
    }
  }
  return config['area'];
}

/**
 * 
 * @param {type} config
 * @returns {unresolved}
 */
function buildAreaDerived(config) {
  if (!config.hasOwnProperty('area_derived')) {
    throw new Error("config doesnot have area_derived key");
  }
  for(var i in CONFIG['position']) {
    pos = CONFIG['position'][i];
    if(config['area_derived'].hasOwnProperty(pos)) {
      config['area_derived'][pos] = _.map(config['area_derived'][pos], function(num) {
        return num * CONFIG['scale'];
      });
    }
  }
  return config['area_derived'];
}

/**
 * 
 * @param {type} length
 * @returns {Array|generate_random_numbers.random_numbers}
 */
function generate_random_numbers(length, random_numbers)
{
  for(var i = 0; i < length; i++) {
    var rand = Math.random() * 10000;
    if (random_numbers.indexOf(rand) == -1) {
      random_numbers.push(rand);
    }
    return random_numbers;
  }
}

/**
 * 
 * @param {type} obj
 * @returns {Array}
 */
function generateUniqueRandomNumbers(obj)
{
  var random_numbers = [];
  for (var i in obj) {
    if (!_.isArray(obj[i])) {
      random_numbers = generate_random_numbers(1, random_numbers);
    } else {
      for (var j in obj[i]) {
        random_numbers = generate_random_numbers(obj[i].length, random_numbers);
      }
    }
  }
  return random_numbers;
}

/**
 * 
 * @param {type} config
 * @returns {unresolved}
 */
function buildAccessOrder(config)
{
  var order = {};
  if (_.keys(config['area']).length > 0) {
    var random_numbers_area = generateUniqueRandomNumbers(config['area']), index = 0;
    for (var i in config['area']) {
      if (!_.isArray(config['area'][i])) {
        order[CONFIG['order']['area'] + "_" + i + "_" + random_numbers_area[index]] = config['area'][i] * CONFIG['scale'];
        index++;
      } else {
        for (var j in config['area'][i]) {
          order[CONFIG['order']['area'] + "_" + i + "_" + random_numbers_area[index]] = config['area'][i][j] * CONFIG['scale'];
          index++;
        }
      }
    }
  }
  if (_.keys(config['area_derived']).length > 0) {
    var random_numbers_area = generateUniqueRandomNumbers(config['area_derived']), index = 0;
    for (var i in config['area_derived']) {
      if (!_.isArray(config['area_derived'][i])) {
        order[CONFIG['order']['area_derived'] + "_" + i + "_" + random_numbers_area[index]] = config['area_derived'][i] * CONFIG['scale'];
        index++;
      } else {
        for (var j in config['area_derived'][i]) {
          order[CONFIG['order']['area_derived'] + "_" + i + "_" + random_numbers_area[index]] = config['area_derived'][i][j] * CONFIG['scale'];
          index++;
        }
      }
    }
  }
  return order;
}

/**
 * 
 * @param {type} config
 * @returns {Array|buildPositions.positions}
 */
function buildPositions(config)
{
  var positions = _.keys(config['area']);
  if (_.keys(config['area_derived']).length > 0) {
    for (var i in config['area_derived']) {
      positions.push(i);
    }
  }
  return positions;
}

/**
 * Assuming all access elements in the same position, also a verification method
 * @param {fabric.Door[]} accessElements
 * @param {string} position
 * @returns {'start': [], 'pivot': []}
 */
function preRenderPlaceAccessByPosition(accessElements, position)
{
  if (accessElements.length > 0) {
    var start_array = [], pivot_array = [], startn_array = null, pivotn_array = null;
    for (var i = 0; i < accessElements.length; i++) {
      start_array.push({
        start: accessElements[i].get('start' + CONFIG['position'][position]),
        pos: i
      });
      pivot_array.push({
        pivot: accessElements[i].get('pivot' + CONFIG['position'][position]),
        pos: i
      });
    }
    startn_array = _.sortBy(start_array, 'start');
    pivotn_array = _.sortBy(pivot_array, 'pivot');
    return {'start': startn_array, 'pivot': pivotn_array};
  } else {
    throw new Error("access elements do not contain any element in the given position");
  }
}

/**
 * 
 * @param {fabric.Canvas} canvas
 * @param {fabric.AccessElement[]} objects
 * @param Array q
 * @returns {undefined}
 */
function placeAccessByPosition(canvas, boundedArea, objects, q)
{
  if (_.keys(objects).length > 0) {
    var obj = null;
    for (var i = 0; i < objects['pivot'].length; i++) {
      obj = q[objects['pivot'][i].pos], position = obj.get('position');
      attr = CONFIG['position'][obj.get('position')];
      otherattr = CONFIG['altposition'][obj.get('position')];
      if(obj.get('type') == "door") {
        placeDoorByRetention(obj, boundedArea);
      } else if(obj.get('type') == "access_element") {
        placeAccessElementByRetention(obj, boundedArea);
      }
      canvas.add(obj);
    }
  }
}

/**
 * 
 * @param {*} obj 
 * @param {fabric.Rect} boundedArea 
 */
function placeAccessElementByRetention(obj, boundedArea)
{
  var position = obj.get('position');
  if(position == "north") {
    obj.set('top', boundedArea.get('top'));
  } else if(position == "south") {
    obj.set('top', boundedArea.get('top') + boundedArea.get('height'));
  } else if(position == "west") {
    obj.set('left', boundedArea.get('left'));
  } else {
    obj.set('left', boundedArea.get('left') + boundedArea.get('width'));
  }
}

/**
 * 
 * @param {*} obj 
 * @param {fabric.Rect} boundedArea 
 */
function placeDoorByRetention(obj, boundedArea)
{
  var position = obj.get('position'), attr = null, otherattr = null, coord = null, position;
  attr = CONFIG['position'][obj.get('position')];
  otherattr = CONFIG['altposition'][obj.get('position')];
  if(CONFIG['altposition'][position]) {
    coord = obj.get('left');
    if(obj.get('startAngle') < 0) {
      obj.set('left', coord + obj.get('start')[attr]);
    } else {
      obj.set('left', coord + obj.get('pivot')[attr]);
    }
  } else {
    coord = obj.get('top');
    if(obj.get('startAngle') < 0) {
      obj.set('top', coord + obj.get('start')[attr]);
    } else {
      obj.set('top', coord + obj.get('pivot')[attr]);
    }
  }
  if(CONFIG['altposition'][position] && (obj.get('retention') < 0)) {
    if(position == "north") {
      obj.set('top', boundedArea.get('top') - obj.get('height')/2);
    } else {
      obj.set('top', boundedArea.get('top') + boundedArea.get('height'));
    }
  } else if(CONFIG['altposition'][position] && (obj.get('retention') > 0)) {
    if(position == "north") {
      obj.set('top', boundedArea.get('top'));
    } else {
      obj.set('top', boundedArea.get('top') + boundedArea.get('height')
      - obj.get('height')/2);
    }
  } else if(CONFIG['position'][position] && (obj.get('retention') > 0)) {
    if(position == "west") {
      obj.set('left', boundedArea.get('left'));
    } else {
      obj.set('left', boundedArea.get('left') + boundedArea.get('width')
      - obj.get('width')/2);
    }
  } else if(CONFIG['position'][position] && (obj.get('retention') < 0)) {
    if(position == "west") {
      obj.set('left', boundedArea.get('left') - obj.get('width')/2);
    } else {
      obj.set('left', boundedArea.get('left') + boundedArea.get('width'));
    }
  }
}

/**
 * 
 * @param {fabric.Door[]} accessElements
 * @returns Array
 */
function aggregateAccessElementsByPosition(accessElements)
{
  var positions = {};
  for (var i in CONFIG['positions']) {
    positions[CONFIG['positions'][i]] = [];
  }
  if (accessElements.length > 0) {
    for (var i = 0; i < accessElements.length; i++) {
      positions[accessElements[i].get('position')].push(i);
    }
    return positions;
  } else {
    throw new Error("access elements do not contain any element for the provided arguments");
  }
}

/**
 * 
 * @param {fabric.Door[]} door
 * @param {fabric.AccessElement[]} w
 * @returns {undefined}
 */
function combineAccessElements(d, w)
{
  var accessElements = d;
  if (w.length > 0) {
    for (var i in w) {
      accessElements.push(w[i]);
    }
  }
  return accessElements;
}

/**
 * 
 * @param {type} id
 * @param {type} fillColor
 * @param {type} data
 * @param {type} config
 * @returns {undefined}
 */
function drawCanvas(id, fillColor, data, config)
{
  canvas = new fabric.Canvas(id);
  canvas.setWidth(data['dim1'] * CONFIG['scale'] + CONFIG['offset']);
  canvas.setHeight(data['dim2'] * CONFIG['scale'] + CONFIG['offset']);
  var width = data['dim1'] * CONFIG['scale'], height = data['dim2'] * CONFIG['scale'],
  figureDimensions = {width: width, height: height},
  rect = createBoundedArea(canvas, {width: canvas.get('width'), height: canvas.get('height')}, fillColor, figureDimensions), 
  result = createAccessForRect(rect, buildArea(config), buildAreaDerived(config), buildAccessOrder(config), buildPositions(config), figureDimensions), 
  accessElements = combineAccessElements(result['door'], result['windowElement']), positions = null, objects = null, 
  positions = aggregateAccessElementsByPosition(accessElements);
  canvas.add(rect);
  for (var i in positions) {
    try {
      var q = [];
      if(positions[i].length > 0) {
        for (var j = 0; j < positions[i].length; j++) {
          q.push(accessElements[positions[i][j]]);
        }
        objects = preRenderPlaceAccessByPosition(q, i);
        placeAccessByPosition(canvas, rect, objects, q);
      }
    } catch (error) {
      console.debug('debug place access');
      console.error(error);
      break;
    }
  }
}

(function($) {
  $(document).ready(function () {
    $canvasElements = $('canvas.fabric-ds');
    var data = null;
    $canvasElements.each(function (index, element) {
      data = $(element).data();
      drawCanvas($(element).data('id'), $(element).data('rgb'), data, data['config']);
    });
  });
})(window.jQuery);
