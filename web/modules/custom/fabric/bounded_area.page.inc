<?php

/**
 * @file
 * Contains bounded_area.page.inc.
 *
 * Page callback for Bounded area entities.
 */

use Drupal\Core\Render\Element;

/**
 * Prepares variables for Bounded area templates.
 *
 * Default template: bounded_area.html.twig.
 *
 * @param array $variables
 *   An associative array containing:
 *   - elements: An associative array containing the user information and any
 *   - attributes: HTML attributes for the containing element.
 */
function template_preprocess_bounded_area(array &$variables) {
  // Fetch BoundedArea Entity Object.
  $bounded_area = $variables['elements']['#bounded_area'];

  // Helpful $content variable for templates.
  foreach (Element::children($variables['elements']) as $key) {
    $variables['content'][$key] = $variables['elements'][$key];
  }
}
