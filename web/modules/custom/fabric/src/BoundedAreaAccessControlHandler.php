<?php

namespace Drupal\fabric;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Bounded area entity.
 *
 * @see \Drupal\fabric\Entity\BoundedArea.
 */
class BoundedAreaAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    /** @var \Drupal\fabric\Entity\BoundedAreaInterface $entity */
    switch ($operation) {
      case 'view':
        if (!$entity->isPublished()) {
          return AccessResult::allowedIfHasPermission($account, 'view unpublished bounded area entities');
        }
        return AccessResult::allowedIfHasPermission($account, 'view published bounded area entities');

      case 'update':
        return AccessResult::allowedIfHasPermission($account, 'edit bounded area entities');

      case 'delete':
        return AccessResult::allowedIfHasPermission($account, 'delete bounded area entities');
    }

    // Unknown operation, no opinion.
    return AccessResult::neutral();
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermission($account, 'add bounded area entities');
  }

}
