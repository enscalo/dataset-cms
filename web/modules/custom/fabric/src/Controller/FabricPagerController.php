<?php

namespace Drupal\fabric\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityManager;
use Drupal\Core\Database\Connection;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Drupal\Core\Url;
use Drupal\fabric\Entity\BoundedArea;

/**
 * Returns responses for Fabric routes.
 */
class FabricPagerController extends ControllerBase {

  /**
   * The example service.
   *
   * @var \Drupal\Core\Entity\EntityManager
   */
  protected $entityManager;
  
  /**
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * Constructs the controller object.
   *
   * @param \Drupal\example\ExampleInterface $example
   *   The example service.
   */
  public function __construct(EntityManager $entityManager, Connection $connection) {
    $this->entityManager = $entityManager;
    $this->connection = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'), $container->get('database')
    );
  }

  /**
   * Builds the response.
   */
  public function boundedAreaPager($filter_key, $sort_key, $page_id) {
    $settings = \Drupal::config('block.block.fabric_fabricblockcaseview')->get('settings');
    $cols = isset($settings['number_of_columns']) ? $settings['number_of_columns'] : $settings['cols'][0];
    $rows = isset($settings['number_of_rows']) ? $settings['number_of_rows'] : $settings['rows'][0];
    $sort_array = array('access' => 'Number_of_Windows', 'area' => 'Volume');
    $filter_array = array_map(function($type) {
      return strtolower($type);
    }, BoundedArea::BOUNDEDAREA_TYPES);
    $filter_array = array_combine($filter_array, BoundedArea::BOUNDEDAREA_TYPES);
    $connection = Database::getConnection("default", "external");
    $count = $rows * $cols;
    $offset = ($page_id - 1) * $count;
    
    $query = $connection->query("SELECT DISTINCT(ba.Entity_Label), ba.id, bal.R, bal.G, bal.B
      FROM app_bounded_area ba 
      INNER JOIN app_bounded_area_labels bal ON bal.Entity_BoundedArea_Fourier = ba.Entity_Label
      WHERE ba.BoundedAreaType = '{$filter_array[$filter_key]}' 
      LIMIT {$count} OFFSET {$offset}");
      
    $query->execute();
    $result = $query->fetchAll(\PDO::FETCH_ASSOC);
    
    $canvas_labels = [];
    for($i = 0; $i < $count; $i++) {
      array_push($canvas_labels, $result[$i]['id']);
    }
    $canvas_elements = [];
    for($i = 0; $i < $count; $i++) {
      array_push($canvas_elements, $result[$i]);
    }
    
    $build = [
      '#type' => 'pager',
      '#theme' => 'fabrichierarchy',
      '#entity_type' => 'bounded_area',
      '#elements' => $canvas_elements,
      '#canvas_block' => array_chunk($canvas_labels, $cols),
      '#rows' => $rows,
      '#cols' => $cols,
    ];
    
    return $build;
  }
  
  public function boundedAreaPlaceAccess(Request $request)
  {
    $filename = $request->get("filename");
    $result = array();
    $filePath = "/tmp/{$filename}.json";
    if(file_exists($filePath)) {
      $cwd = getcwd();
      $directory = array(dirname(dirname(DRUPAL_ROOT)), "serverless-py3");
      chdir(implode(DIRECTORY_SEPARATOR, $directory));
      $command = '/bin/bash -c "serverless invoke local --function place_access --path ' . $filePath . '"';
      exec($command, $output, $return_var);
      $result = json_decode(implode($output, ''), true);
      chdir($cwd);
    } else {
      return new JsonResponse(array(), 200);
    }
    return new JsonResponse($result, 200);
  }
  
  public function boundedAreaAlternatives($entity_id, $num_alternatives)
  {
    $connection = Database::getConnection("default", "external");
    $rows = $num_alternatives;
    $cols = 1;
    $query = $connection->select('app_bounded_area_labels', 'bal')->fields('ba', array('id', 'Entity_Label'))
    ->fields('bal', array('R', 'G', 'B'));
    $query->innerJoin("app_bounded_area", 'ba', 'bal.Entity_BoundedArea_Fourier = ba.Entity_Label');
    $query->condition("ba.id", $entity_id, '=');
    $result = $query->execute()->fetchAll(\PDO::FETCH_ASSOC);
    
    $canvas_labels = [];
    for($i = 0; $i < $rows; $i++) {
      array_push($canvas_labels, $result[0]['id']);
    }
    $canvas_elements = [];
    for($i = 0; $i < $rows; $i++) {
      array_push($canvas_elements, $result[0]);
    }
    
    $build = [
      '#type' => 'pager',
      '#theme' => 'fabrichierarchy',
      '#entity_type' => 'bounded_area',
      '#elements' => $canvas_elements,
      '#canvas_block' => array_chunk($canvas_labels, $cols),
      '#rows' => $rows,
      '#cols' => $cols,
    ];
    
    return $build;
  }

}
