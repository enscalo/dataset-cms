<?php

namespace Drupal\fabric;

use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\Sql\SqlContentEntityStorageSchema;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the user schema handler.
 */
class BoundedAreaStorageSchema extends SqlContentEntityStorageSchema {
    
}
