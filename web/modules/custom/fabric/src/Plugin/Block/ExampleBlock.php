<?php

namespace Drupal\fabric\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Example' block.
 *
 * @Block(
 *   id = "fabric_example",
 *   admin_label = @Translation("Example"),
 *   category = @Translation("Fabric")
 * )
 */
class ExampleBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
