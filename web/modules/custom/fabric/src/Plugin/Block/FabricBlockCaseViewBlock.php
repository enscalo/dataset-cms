<?php

namespace Drupal\fabric\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Block\MainContentBlockPluginInterface;

/**
 * Provides a 'FabricBlockCaseView' block.
 *
 * @Block(
 *   id = "fabric_fabricblockcaseview",
 *   admin_label = @Translation("FabricBlockCaseView"),
 *   category = @Translation("Custom")
 * )
 */
class FabricBlockCaseViewBlock extends BlockBase implements MainContentBlockPluginInterface {

  private $main_content;
  
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'cols' => range(1,5),
      'rows' => range(5,18)
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $config = $this->defaultConfiguration();
    $form['number_of_columns'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of Columns'),
      '#options' => array_combine($config['cols'], $config['cols']),
      '#default_value' => $this->configuration['number_of_columns']
    ];
    $form['number_of_rows'] = [
      '#type' => 'select',
      '#title' => $this->t('Number of Rows'),
      '#options' => array_combine($config['rows'], $config['rows']),
      '#default_value' => $this->configuration['number_of_rows']
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['number_of_columns'] = $form_state->getValue('number_of_columns');
    $this->configuration['number_of_rows'] = $form_state->getValue('number_of_rows');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $build['content'] = [
      '#markup' => array(
        'canvas_case_block' => $this->build_block()
      ),
      '#allowed_tags' => [
        'canvas', 
        'div',
        'p'
      ]
    ];
    return $build;
  }
  
  /**
   * 
   */
  public function build_block() {
    $config = $this->getConfiguration();
    $block_array = [
      '#type' => 'container',
      'content' => []
    ];
    for($j = 0; $j < (int) $config['number_of_rows']; $j++) {
      $block = [
        '#type' => 'container',
        '#markup' => [
          'content' => [],
        ]
      ];
      for($i = 0; $i < (int) $config['number_of_columns']; $i++) {
        array_push($block['#markup']['content'], array(
          '#type' => 'container',
          '#entity' => $this->main_content['entity_ids_chunk'][$j][$i],
          '#entity_type' => $this->main_content['entity_type']
        ));
      }
      array_push($block_array['content'], $block);
    }
    return $block_array;
  }

  public function setMainContent(array $main_content) {
    $this->main_content = $main_content;
  }

}
