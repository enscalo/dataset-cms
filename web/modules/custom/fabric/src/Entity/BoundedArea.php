<?php

namespace Drupal\fabric\Entity;

use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeInterface;

/**
 * Defines the Bounded area entity.
 *
 * @ingroup fabric
 *
 * @ContentEntityType(
 *   id = "bounded_area",
 *   label = @Translation("Bounded area"),
 *   handlers = {
 *     "storage" = "Drupal\fabric\BoundedAreaStorage",
 *     "storage_schema" = "Drupal\fabric\BoundedAreaStorageSchema",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "list_builder" = "Drupal\fabric\BoundedAreaListBuilder",
 *     "views_data" = "Drupal\fabric\Entity\BoundedAreaViewsData",
 *
 *     "form" = {
 *       "default" = "Drupal\fabric\Form\BoundedAreaForm",
 *       "add" = "Drupal\fabric\Form\BoundedAreaForm",
 *       "edit" = "Drupal\fabric\Form\BoundedAreaForm",
 *       "delete" = "Drupal\fabric\Form\BoundedAreaDeleteForm",
 *     },
 *     "access" = "Drupal\fabric\BoundedAreaAccessControlHandler",
 *     "route_provider" = {
 *       "html" = "Drupal\fabric\BoundedAreaHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "app_bounded_area",
 *   data_table = "app_bounded_area",
 *   admin_permission = "administer bounded area entities",
 *   entity_keys = {
 *     "id" = "id"
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/bounded_area/{bounded_area}",
 *     "add-form" = "/admin/structure/bounded_area/add",
 *     "edit-form" = "/admin/structure/bounded_area/{bounded_area}/edit",
 *     "delete-form" = "/admin/structure/bounded_area/{bounded_area}/delete",
 *     "collection" = "/admin/structure/bounded_area",
 *   }
 * )
 */
class BoundedArea extends ContentEntityBase implements ContentEntityInterface {

  const ACCESS_HEIGHT = 8 * 12 * 2.54 / 100;
  const LOW_ISOLATED = 'Low_Isolated';
  const LOW_ACCESSIBLEAREA = 'Low_AccessibleArea';
  const HIGH_ISOLATED = 'High_Isolated';
  const HIGH_ACCESSIBLEAREA = 'High_AccessibleArea';
  const PRE_PLANCOMPONENT = 'Pre_PlanComponent';
  const POST_PLANCOMPONENT = 'Post_PlanComponent';
  
  const BOUNDEDAREA_TYPES = array(BoundedArea::LOW_ISOLATED, BoundedArea::LOW_ACCESSIBLEAREA, BoundedArea::HIGH_ISOLATED, BoundedArea::HIGH_ACCESSIBLEAREA, BoundedArea::PRE_PLANCOMPONENT, BoundedArea::POST_PLANCOMPONENT);
  
  /**
   * 
   * @return array
   */
  public function getWindows() {
    $values = [(float) $this->get('W1')->value, (float) $this->get('W2')->value, 
      (float) $this->get('W3')->value, (float) $this->get('W4')->value, (float) $this->get('W5')->value];
    return array_filter($values);
  }
  
  public function getAccess() {
    return [(float) $this->get('Area_derived')->value];
  }
  
  public function getDim1() {
    return (float) $this->get('Dim1')->value;
  }
  
  public function getDim2() {
    return (float) $this->get('Dim2')->value;
  }
  
  public function getNumberOfAccesses() {
    return (int) $this->get('Number_of_Accesses')->value;
  }
  
  public function getNumberOfWindows() {
    return (int) $this->get('Number_of_Windows')->value;
  }
  
  public function toArray() {
    $values = parent::toArray();
    $values['windows'] = $this->getWindows();
    $values['accesses'] = $this->getAccess();
    return $values;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['id'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('ID'))
      ->setDescription(t('The ID of the Contact entity.'))
      ->setReadOnly(TRUE);
    
    $fields['BoundedAreaType'] = BaseFieldDefinition::create('string')
      ->setLabel(t('BoundedAreaType'))
      ->setDescription(t('Bounded Area Type'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['LayoutType'] = BaseFieldDefinition::create('string')
      ->setLabel(t('LayoutType'))
      ->setDescription(t('Layout type'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['x'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Neighborhood X'))
      ->setDescription(t('Neighborhood value x coordinate'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['y'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Neighborhood Y'))
      ->setDescription(t('Neighborhood value y coordinate'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['xsize'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Neighborhood X'))
      ->setDescription(t('Neighborhood value xsize coordinate'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['ysize'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Neighborhood X'))
      ->setDescription(t('Neighborhood value ysize coordinate'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['Number_of_Windows'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of Windows'))
      ->setDescription(t('Windows in the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['Number_of_Accesses'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Number of Accesses'))
      ->setDescription(t('Accesses in the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['W1'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Window 1'))
      ->setDescription(t('Window in the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['W2'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Window 1'))
      ->setDescription(t('Window in the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['W3'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Window 1'))
      ->setDescription(t('Window in the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['W4'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Window 1'))
      ->setDescription(t('Window in the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['W5'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Window 1'))
      ->setDescription(t('Window in the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['Area_derived'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Access area'))
      ->setDescription(t('Access area in the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['Volume'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Floor Area'))
      ->setDescription(t('Floor area of the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['Dim1'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Dimension 1'))
      ->setDescription(t('Dimension 1 of the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);
    
    $fields['Dim2'] = BaseFieldDefinition::create('float')
      ->setLabel(t('Dimension 2'))
      ->setDescription(t('Dimension 2 of the bounded area'))
      ->setRevisionable(FALSE)
      ->setTranslatable(FALSE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => 0,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['Entity_Label'] = BaseFieldDefinition::create('decimal')
      ->setLabel(t('Entity label'))
      ->setDescription(t('Label for the bounded area entity'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['Entity_Class'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Entity class'))
      ->setDescription(t('Class for the bounded area entity'))
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'hidden',
        'weight' => -4,
      ])
      ->setDisplayConfigurable('view', TRUE);

    return $fields;
  }

}
