<?php

namespace Drupal\fabric\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Bounded area entities.
 */
class BoundedAreaViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $data = parent::getViewsData();

    // Additional information for Views integration, such as table joins, can be
    // put here.

    return $data;
  }

}
