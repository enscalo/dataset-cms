<?php

namespace Drupal\fabric\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Bounded area entities.
 *
 * @ingroup fabric
 */
class BoundedAreaDeleteForm extends ContentEntityDeleteForm {


}
