<?php

namespace Drupal\fabric;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityListBuilder;
use Drupal\Core\Link;

/**
 * Defines a class to build a listing of Bounded area entities.
 *
 * @ingroup fabric
 */
class BoundedAreaListBuilder extends EntityListBuilder {


  /**
   * {@inheritdoc}
   */
  public function buildHeader() {
    $header['id'] = $this->t('Bounded area ID');
    return $header + parent::buildHeader();
  }

  /**
   * {@inheritdoc}
   */
  public function buildRow(EntityInterface $entity) {
    /* @var $entity \Drupal\fabric\Entity\BoundedArea */
    $row['id'] = $entity->id();
    $row['name'] = Link::createFromRoute(
      $entity->label(),
      'entity.bounded_area.edit_form',
      ['bounded_area' => $entity->id()]
    );
    return $row + parent::buildRow($entity);
  }

}
